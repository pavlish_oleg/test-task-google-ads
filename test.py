from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/postdata', methods=['POST'])
def post_data():
    if request.method == 'POST':
        data = request.json
        print("Отримано дані:", data)
        return jsonify({'message': 'Дані успішно отримано'}), 200
    else:
        return jsonify({'error': 'Метод не підтримується'}), 405

if __name__ == '__main__':
    app.run(debug=True)
