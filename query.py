def get_main_query(start_date, end_date):
    main_query = f"""
            SELECT
                campaign.id,
                campaign.name,
                customer.currency_code,
                campaign.status,
                campaign.bidding_strategy_type,
                campaign.advertising_channel_type,
                segments.date,
                customer.descriptive_name,
                customer.id,
                ad_group.id,
                ad_group.name,
                ad_group_ad.ad.id,
                ad_group_ad.ad.name,
                metrics.clicks,
                metrics.impressions,
                metrics.conversions,
                metrics.average_cost,
                metrics.average_cpc,
                metrics.average_cpm,
                metrics.ctr,
                ad_group_ad.ad.final_urls,
                segments.device,
                segments.keyword.info.text
            FROM
                ad_group_ad
            WHERE
                segments.date BETWEEN '{start_date}' AND '{end_date}'
            ORDER BY
                segments.date
        """
    return main_query