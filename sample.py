import requests, time, asyncio, httpx, query, json
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from fastapi import FastAPI
from pydantic import BaseModel
from typing import List, Optional
from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

app = FastAPI()

class Creds(BaseModel):
    developer_token: str
    use_proto_plus: bool
    validate_only: bool
    client_id: str
    client_secret: str
    refresh_token: str

class InputData(BaseModel):
    client_ids: List[str]
    period: str
    user_uid: str
    creds: Creds
    callback_url: Optional[str] = None


@app.get("/")
def read_root():
    return {"message": "My Google Ads Scraper. Hi there!"}


async def get_past_date(units_time):
    n = int(units_time.split('_')[0])
    units = units_time.split('_')[1]
    
    current_date = datetime.now()
    if units == 'days':
        past_date = current_date - timedelta(days=n)
    elif units == 'weeks':
        past_date = current_date - timedelta(weeks=n)
    elif units == 'months':
        past_date = current_date - relativedelta(months=n)
    elif units == 'years':
        past_date = current_date - relativedelta(years=n)
    else:
        raise ValueError("Непідтримувана одиниця часу")
        
    return past_date.strftime('%Y-%m-%d')

async def fetch_data(service, custom_client, main_query):
    try:
        response = service.search(customer_id=custom_client, query=main_query)
        result = []
        for row in response:
            try:
                utms = row.ad_group_ad.ad.final_urls[0].split('/?')[1].split('&')
                utm_source = next((source.split('=')[1] for source in utms if 'utm_source' in source), None)
                utm_medium = next((medium.split('=')[1] for medium in utms if 'utm_medium' in medium), None)
                utm_campaign = next((campaign.split('=')[1] for campaign in utms if 'utm_campaign' in campaign), None)
                utm_content = next((content.split('=')[1] for content in utms if 'utm_content' in content), None)
                utm_term = next((term.split('=')[1] for term in utms if 'utm_term' in term), None)
            except:
                utm_source, utm_medium, utm_content, utm_campaign, utm_term = None, None, None, None, None
            result.append({
                "cmp_id": row.campaign.id,
                "cmp_name": row.campaign.name,
                "currency": row.customer.currency_code,
                "cmp_status": row.campaign.status,
                "cmp_bidding_strategy": row.campaign.bidding_strategy_type,
                "cmp_ad_channel": row.campaign.advertising_channel_type,
                "date_impression": row.segments.date,
                "ad_group_id": row.ad_group.id,
                "ad_group_name": row.ad_group.name,
                "ad_id": row.ad_group_ad.ad.id,
                "ad_name": row.ad_group_ad.ad.name,
                "keyword": row.segments.keyword.info.text,
                "ad_clicks": row.metrics.clicks,
                "ad_impressions": row.metrics.impressions,
                "ad_conversions": row.metrics.conversions,
                "ad_cost": row.metrics.average_cost,
                "ad_cpc": row.metrics.average_cpc,
                "ad_cpm": row.metrics.average_cpm,
                "ad_ctr": row.metrics.ctr,
                "url": row.ad_group_ad.ad.final_urls[0],
                "utm_source": utm_source,
                "utm_medium": utm_medium,
                "utm_campaign": utm_campaign,
                "utm_content": utm_content,
                "utm_term": utm_term,
                "device": row.segments.device
            })
        return {'id': custom_client, "metrics": result}
    except GoogleAdsException as e:
        return {'id': custom_client, "error": e.failure.errors[0].message}

@app.post("/scrape")
async def scrape(data: InputData):
    start_time = time.time()
    try:
        start_date = await get_past_date(data.period)
        print(start_date)
        print(type(start_date))
        end_date = datetime.now().strftime('%Y-%m-%d')
        print(end_date)
        main_query = query.get_main_query(start_date, end_date)
        credentials = {
            "developer_token": data.creds.developer_token,
            "refresh_token": data.creds.refresh_token,
            "client_id": data.creds.client_id,
            "use_proto_plus": data.creds.use_proto_plus,
            "client_secret": data.creds.client_secret,
        }
        client = GoogleAdsClient.load_from_dict(credentials)
        service = client.get_service("GoogleAdsService")

        tasks = [fetch_data(service, client_id, main_query) for client_id in data.client_ids]
        results = await asyncio.gather(*tasks)

        all_clients = [result for result in results if "metrics" in result]
        faulty_clients = [result['id'] for result in results if "error" in result]
        message = 'Empty list was provided' if not all_clients else 'Data successfully scraped!'

        jska = {
            "success": bool(all_clients),
            "message": message,
            "start_date": start_date,
            "end_date": end_date,
            "succeded_clients": all_clients,
            "faulty_clients": faulty_clients,
            "user_uid": data.user_uid,
            "service": 'gads',
            "time": time.time() - start_time
        }
        if data.callback_url:
            async with httpx.AsyncClient() as client:
                print(message)
                jska = json.dumps(jska)
                await client.post(data.callback_url, json=jska, timeout=60)
    except Exception as e:
        jska = {
            "success": False,
            "message": str(e),
            "user_uid": data.user_uid,
            "service": "gads"
        }
        if data.callback_url:
            async with httpx.AsyncClient() as client:
                print(jska)  # Print jska before sending
                await client.post(data.callback_url, json=jska)
        return jska
    return jska
